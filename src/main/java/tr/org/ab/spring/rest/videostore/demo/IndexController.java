package tr.org.ab.spring.rest.videostore.demo;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/")
public class IndexController {

    @RequestMapping("/test")
    TextResponse hello(){
        //return "hello";
        //return Collections.singletonMap("message", "hello world");
        TextResponse response = new TextResponse();
        //response.setMessage("hello world");
        response.setName("halit");
        response.setStatus(false);
        response.setItems(Arrays.asList("ubuntu","audi"));

        return response;

    }
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class TextResponse{
        @JsonProperty("renamedProperty")
        private boolean status;
        private String name;
        private String message;
        private List<String> items;

        public boolean isStatus() {
            return status;
        }

        public void setStatus(boolean status) {
            this.status = status;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<String> getItems() {
            return items;
        }

        public void setItems(List<String> items) {
            this.items = items;
        }
    }
}
